<div class="wrap">
<h2><?php echo __("Deal Series"); ?></h2>
<p>Below is a list of deal series to which subscribers can be subscribed. To define a new deal series use the form below. 
<p><strong>Note:</strong> Deal series attached to deleted categories will not be processed by the background process. The subscribers of those series will not receive any email. Assign a new category to those deal series to resume processing them. 
<p><!-- list of deal series starts here -->



<div align="right"><a class="button-primary" href="#create">Create Post Series</a></div>
</p>
<table class="widefat">
	<thead>
	<tr> 
	    <th><?php echo __("ID"); ?></th>
	    <th><?php echo __("Series Name"); ?></th>
	    <th><?php echo __("Active Subscribers"); ?></th>
	    <th><?php echo __("Unsubscribed Subscribers"); ?></th>
	    <th><?php echo __("Deal Category "); ?></th>
	    <th><?php echo __("Periodicity"); ?></th>
	    <th><?php echo __("Edit"); ?></th>
	    <th><?php echo __("Delete"); ?></th>
	</tr>
	</thead>
	<?php
	//show a list of deal series
	if (count($deal_info))
        {    
	foreach ($deal_info as $deal)
	{
	?>
	<tr>
	  <td><?php echo $deal['Deal']->id; ?></td>
	  <td><?php echo $deal['Deal']->name; ?></td>
	  <td><?php echo $deal['Active']; ?></td>
	  <td><?php echo $deal['Unsubscribed']; ?></td>
  	  <td><?php 
  	  
          
         $category = null;
         foreach ($deal_categories as $cat)
         {
             if ($cat->ID ==  $deal['Category'])
                 $category = $cat;
         }

  	  if (!$category) {
  	  ?>
  	  <span style="color:red; font-weight: bold">Related category deleted.<p>Assign new category to process</p></span>
  	  <?php
  	  }else
          {
              echo $category->name;
          }
?></td>
 	  	  <td><?php echo $deal['Deal']->period; ?></td>
 	  	  <td><p>
 	  	  <a href="admin.php?page=wpr-deals-bridge/dealsbridge.php&method=edit&id=<?php echo $deal['Deal']->id ?>" class="button-primary">Edit</a>
 	  	  
 	  	  </td>
 	  	   	  	  <td>
                                  <form action="admin.php?page=wpr-deals-bridge/dealsbridge.php" method="post">
                                      <input type="hidden" name="id" value="<?php echo $deal['Deal']->id; ?>"/>
					<input type="hidden" name="wprdb_form" value="deal_series_delete_form"/>
 	  	  <input type="submit" value="Delete"  onclick="return window.confirm('Are you sure you want to continue? Any subscribers who are subscribed to this series will no longer receive any email and CANNOT be moved to another deal series. This action CANNOT be undone. Do you want to continue?');" class="button-primary">
 	  	 
                                  </form>

 	  	  
 	  	  </td>
	</tr>
	<?php
	}
        }
        else
        {
            ?><tr>
                <td colspan="10"><center>--No Deal Series Defined. Please fill out the form below to create one.--</center>
            </tr><?php
        }
	
	?>
</table>
<!-- list of deal series ends here -->
<a name="create"><h3><?php _e("Create Deal Series") ?></h3></a>
<?php
if (count($errors)) { ?>
<div class="error fade">
<?php

 foreach ($errors as $index=>$error) 
{
  
   ?>
   <?php echo $index+1 ?>. <?php echo $error ?><br />
   <?php
}
?>
</ul>
</p>
</div>
<?php 
} ?>


<form action="admin.php?page=wpr-deals-bridge/dealsbridge.php" method="post">

<table>
  <tr>
     <td><strong><?php echo __("Name of Series"); ?>:</strong>
     
     <p><small><?php echo __("Enter the name of the series as will be listed in the subscription form creation interface.") ?></small></p>
     
     </td>
     <td><input type="text" size="40" name="series_name" /></td>
  </tr>
  <tr>
     <td><strong><?php echo __("Select the Category"); ?>:</strong>
     
     <p><small><?php echo __("Select the deal category to which these subscribers will be subscribed."); ?></small></p>
     
     </td>
     <td>
     <select name="category">
     <?php
     
    

     foreach ($deal_categories as $cat)
     {
     ?>
     <option value="<?php echo $cat->ID ?>"><?php echo $cat->name ?></option>
     <?php
     }
     ?>
     </select>
     </td>
  </tr>
    <tr>
     <td><strong><?php echo __("Periodicity") ?>:</strong>
     
     <p><small><?php echo __("Select the periodicity with which subscribers of these series will receive email."); ?></small></p>
     </td>
     <td><input type="text" size="2" value="<?php if (isset($deal->period)) { echo $deal->period; } ?>" name="period" /></td>
  </tr>
  <tr>
    <td><input type="submit" class="button-primary" value="<?php echo __("Create Deal Series"); ?>"/></td>
  </tr>
</div>
<input type="hidden" name="wprdb_form" value="deal_series_create_form"/>

</form>

<script>
<?php if (count($errors) > 0)
{
?>
window.location='#create';
<?php
}
?></script>
