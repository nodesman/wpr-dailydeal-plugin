<div class="wrap">
<?php
if (count($errors)) { ?>
<div class="error fade">
<p>
<ul>
<?php

 foreach ($errors as $index=>$error) 
{
  
   ?>
   <?php echo $index+1 ?>. <?php echo $error ?><br />
   <?php
}
?>
</ul>
</p>
</div>
<?php 
} ?>
<h2>Edit Deal Series</h2>
<form action="<?php echo $_SERVER['REQUEST_URI'] ?>" method="post">
<?php


?>
<table>
  <tr>
     <td><strong><?php echo __("Name of Series"); ?>:</strong>
     
     <p><small><?php echo __("Enter the name of the series as will be listed in the subscription form creation interface.") ?></small></p>
     
     </td>
     <td><input type="text" size="40" name="series_name" value="<?php echo $deal->name ?>" /></td>
  </tr>
  <tr>
     <td><strong><?php echo __("Select the Category"); ?>:</strong>
     <p><small><?php echo __("Select the deal category to which the subscribes of this series will be subscribed."); ?></small></p>
     </td>
     <td>
     <?php
     $category_current = get_term($deal->deal_category,"seller_category");
     
          
     if (NULL == $category_current || is_a($category_current,"WP_Error"))
     {
         $categoryDoesntExist = true;
     }
     
     
     ?>
     <select name="category">
     <?php if ($categoryDoesntExist) { ?> <option value="0" disabled="disabled" selected="selected" >Deleted Category</option><?php } ?>
     <?php
     foreach ($categories as $cat)
     {
     ?>
     <option <?php if ($cat->ID == $deal->deal_category) { ?>selected="selected" <?php } ?> value="<?php echo $cat->ID ?>"><?php echo $cat->name ?></option>
     <?php
     }
     ?>
     </select>
     </td>
  </tr>
    <tr>
     <td><strong><?php echo __("Periodicity") ?>:</strong>
     
     <p><small><?php echo __("Enter the name of the series as will be listed in the subscription form creation interface."); ?></small></p>
     </td>
     <td valign="top">Every <input type="text" size=2 name="period" value="<?php echo $deal->period ?>"/> days</td>
     </td>
  </tr>
  <tr>
    <td><input type="submit" class="button-primary" value="<?php echo __("Save Deal Series"); ?>"/></td>
  </tr>
</div>
<input type="hidden" name="wprdb_form" value="deal_series_edit_form"/>

</form>
