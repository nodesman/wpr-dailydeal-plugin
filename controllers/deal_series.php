<?php


add_action("_wprdb_deal_series_home_handle","_wprdb_deal_series_home");

function _wprdb_deal_series_home()
{
	$method = $_GET['method'];
	
	switch ($method)
	{
	   case 'edit':
	        
	   _wprdb_deal_series_edit();
	   
	   break;
	   
	   case 'delete':
	       
	   
	   
	   
	   break;
	   
	   default:
		_wprdb_deal_series_default();
	
	}
}

function _wprdb_deal_series_edit()
{
    global $wpdb;
    $did = intval($_GET['id']);
    if (!_wprdb_get("deal"))
    {
	    $getDealQuery = sprintf("SELECT * FROM %sdeal_email_series WHERE id=%d",$wpdb->prefix,$did);    
	    $deal = $wpdb->get_results($getDealQuery);
	    
	    _wprdb_set("deal",$deal[0]);

    }
	    _wprdb_set("_wprdb_view","deal_edit"); 
	     	  $getCategoriesQuery = sprintf("SELECT tax.term_taxonomy_id ID, t.name FROM %sterm_taxonomy tax, %sterms t WHERE tax.taxonomy='seller_category' AND tax.term_id=t.term_id;",$wpdb->prefix,$wpdb->prefix);
	    $categories = $wpdb->get_results($getCategoriesQuery);
   	    _wprdb_set("categories",$categories); 

}


function _wprdb_deal_series_default()
{
//get a list of deal series currenlty defined in the database. 
	global $wpdb;
	$getDealSeriesQuery = sprintf("SELECT * FROM %sdeal_email_series",$wpdb->prefix);
	$deal_series = $wpdb->get_results($getDealSeriesQuery);
	
	$deals_info = array();
		
	//find out how many are subscribed.
	foreach ($deal_series as $deal)
	{
	   $getDealSubscribedQuery = sprintf("SELECT COUNT(*) num FROM %sdeal_email_subscriptions dealsub, %swpr_subscribers sub WHERE dealsub.sid=sub.id AND dealsub.deal_category=%d AND sub.active=1 AND sub.confirmed=1",$wpdb->prefix,$wpdb->prefix,$deal->deal_category);
	   $dealsCountResult = $wpdb->get_results($getDealSubscribedQuery);	
	   $deals_info[$deal->id]['Active'] = $dealsCountResult[0]->num;
	   $deals_info[$deal->id]['Deal'] = $deal;
	}
	
	//find out how many are unsubscribed.	
	foreach ($deal_series as $deal)
	{
	   $getDealSubscribedQuery = sprintf("SELECT COUNT(*) num FROM %sdeal_email_subscriptions dealsub, %swpr_subscribers sub WHERE dealsub.sid=sub.id AND dealsub.deal_category=%d AND sub.active=0 AND sub.confirmed=1",$wpdb->prefix,$wpdb->prefix,$deal->deal_category);
	   $dealsCountResult = $wpdb->get_results($getDealSubscribedQuery);	
	   $deals_info[$deal->id]['Unsubscribed'] = $dealsCountResult[0]->num;
	}
               
	foreach ($deal_series as $deal)
	{
           $deals_info[$deal->id]['Category'] = $deal->deal_category;
	}
	
	    $getCategoriesQuery = sprintf("SELECT tax.term_taxonomy_id ID, t.name FROM %sterm_taxonomy tax, %sterms t WHERE tax.taxonomy='seller_category' AND tax.term_id=t.term_id;",$wpdb->prefix,$wpdb->prefix);

	    $categories = $wpdb->get_results($getCategoriesQuery);
	
	_wprdb_set("deal_info",$deals_info);
	_wprdb_set("deal_categories",$categories);				
	
	//ensure that the corresponding category exists.
	_wprdb_set("_wprdb_view","deals_home"); //set the view to the deal series list page. 
}


function _wprdb_deal_series_edit_form_post()
{
	global $wpdb;
	$did = intval($_GET['id']);
	$deal_name = trim($_POST['series_name']);
	
	$deal->name = $deal_name;
	if (empty($deal_name))
	{
	  $error[] = __("The deal name should not be empty");
	  $deal->name = "";
	}
	else if (preg_match("@[^a-zA-Z0-9 ]+@",$deal_name))
	{
		$error[] = __("The deal name has invalid characters");
  	        $deal->name = "";
	}
	else
	{
		//ensure that this name doesn't already exist.
		$checkOtherCategoriesQuery = $wpdb->prepare("SELECT COUNT(*) num FROM {$wpdb->prefix}deal_email_series WHERE name=%s AND id != %d;",$deal_name,$did);
		$result = $wpdb->get_results($checkOtherCategoriesQuery);
		$number = $result[0]->num;
		if ($number != 0)
		{
		   $error[] = __("A deal series with that name already exists");
	   	  $deal->name = "";
		}
	}


	$category = intval($_POST['category']);
 	//ensure that this category isn't the deleted category option.

 	if ($category == 0)
 	{
 	   $error[] = __("You must choose a category which this deal series should deliver");
           $deal->deal_category = '';
 	}
 	else
 	{
 	    $deal->deal_category = $category;
 	}
 	
 	$period = intval($_POST['period']);
 	//ensure that it is a possitive integer.
 	$deal->period = $period;
 	if ($period <=0)
 	{
	 	$error[] = __("The periodicity should be a positive number");
	 	$deal->period = '';
 	}
	    
 	
 	if (0 != count($error))
 	{
 	    _wprdb_set("errors",$error);
 	}
 	else
 	{
 		$updateDealQuery = $wpdb->prepare("UPDATE {$wpdb->prefix}deal_email_series SET `name`=%s, deal_category=%d, period=%d WHERE id=%d",$deal->name, $deal->deal_category, $deal->period, $did);
 		$wpdb->query($updateDealQuery);
 		wp_redirect("admin.php?page=wpr-deals-bridge/dealsbridge.php");
 	
 	}
 	_wprdb_set("deal",$deal);
 	_wprdb_set("categories",$categories);

}

function _wprdb_deal_series_create_form_post()
{
	global $wpdb;
	$did = intval($_GET['id']);
	$deal_name = trim($_POST['series_name']);
	
	$deal->name = $deal_name;
	if (empty($deal_name))
	{
	  $error[] = __("The deal name should not be empty");
	  $deal->name = "";
	}
	else if (preg_match("@[^a-zA-Z0-9 ]+@",$deal_name))
	{
		$error[] = __("The deal name has invalid characters");
  	        $deal->name = "";
	}
	else
	{
		//ensure that this name doesn't already exist.
		$checkOtherCategoriesQuery = $wpdb->prepare("SELECT COUNT(*) num FROM {$wpdb->prefix}deal_email_series WHERE name=%s AND id != %d;",$deal_name,$did);
		$result = $wpdb->get_results($checkOtherCategoriesQuery);
		$number = $result[0]->num;
		if ($number != 0)
		{
		   $error[] = __("A deal series with that name already exists");
	   	  $deal->name = "";
		}
	}


	$category = intval($_POST['category']);
 	//ensure that this category isn't the deleted category option.

 	if ($category == 0)
 	{
 	   $error[] = __("You must choose a category which this deal series should deliver");
           $deal->deal_category = '';
 	}
 	else
 	{
 	    $deal->deal_category = $category;
 	}
 	
 	$period = intval($_POST['period']);
 	//ensure that it is a possitive integer.
 	$deal->period = $period;
 	if ($period <=0)
 	{
	 	$error[] = __("The periodicity should be a positive number");
	 	$deal->period = '';
 	}
 	
 	if (0 != count($error))
 	{
 	    _wprdb_set("errors",$error);
 	}
 	else
 	{
 		$insertDealQuery = $wpdb->prepare("INSERT INTO {$wpdb->prefix}deal_email_series (`name`, deal_category, period) VALUES (%s, %d,  %d)",$deal->name, $deal->deal_category, $deal->period);
 		$wpdb->query($insertDealQuery);
 		wp_redirect("admin.php?page=wpr-deals-bridge/dealsbridge.php");
 	
 	}

 	_wprdb_set("deal",$deal);
}


function _wprdb_deal_series_deletion_form_post()
{
      global $wpdb;
      $id = intval($_POST['id']);
      if (current_user_can("activate_plugins"))
      {
	      $deleteDealSeriesQuery = sprintf("DELETE FROM %sdeal_email_series WHERE id=%d",$wpdb->prefix,$id);
	      $wpdb->query($deleteDealSeriesQuery);
	      wp_redirect("admin.php?page=wpr-deals-bridge/dealsbridge.php");
      }
}

add_action("_wprdb_deal_series_edit_form_post","_wprdb_deal_series_edit_form_post");

add_action("_wprdb_deal_series_create_form_post","_wprdb_deal_series_create_form_post");

add_action("_wprdb_deal_series_delete_form_post","_wprdb_deal_series_deletion_form_post");


add_action("_wprdb_deal_series_create_form_handle","_wprdb_deal_series");

