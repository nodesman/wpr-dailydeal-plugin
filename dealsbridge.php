<?php
/*
Plugin Name: WP Autoresponder To Daily Deals Bridge
Plugin URI: http://www.nodesman.com/
Description: Bridge plugin for integrating WP Autoresponder with Daily Deals Theme
Version: 0.1
Author: Raj Sekharan
Author URI: http://www.nodesman.com/
*/

//this is a test
$plugindir =  str_replace(basename(__FILE__),"",__FILE__);
$plugindir = str_replace('\\','/',$plugindir);
$plugindir = rtrim($plugindir,"/");


include_once "$plugindir/lib/framework.php";
include_once "$plugindir/controllers/deal_series.php";
include_once "$plugindir/cron.php";


define("WPRDB_VERSION","0.1");
define("WPRDB_PLUGIN_DIR","$plugindir");

$wprdb_globals = array();


add_action("admin_init","wprdb_admin_init");
add_action("init","wprdb_init");


function checkWhetherWPRActive()
{
    $pluginsList = get_option("active_plugins");
    $whetherActive = false;
    foreach ($pluginsList as $plugin)
    {
        if (preg_match("@/wpresponder.php$@",$plugin))
        {
            $whetherActive = true;
        }
    }
    return $whetherActive;
}


function wprdb_admin_init()
{
    //ensure that the wp autoresponder plugin is enabled.
    
    $whetherActive = checkWhetherWPRActive();

    if (!$whetherActive)
    {
    	//alert the user that wp autoresponder is no longer active and therefore the e-mails will not be delivered.
        add_action("admin_notices","wprdb_nowpr_found");
    }
    
    $current_theme = get_current_theme();
    if ($current_theme != "DailyDeal")
    {
        //alert the user that wp autoresponder is no longer active and therefore the e-mails will not be delivered.
        add_action("admin_notices","wprdb_nodd_found");
    }
    
    //ensure that the tables for the plugin exists if not alert
}


function wprdb_init()
{
	//ensure that the crons are scheduled
	global $wpdb;
	//create the deal tables
	
	if (isset($_GET['page']) && is_admin() && ($_GET['page'] == 'wpr-deals-bridge/dealsbridge.php'))
	{
		_wprdb_handle_post();
 		_wprdb_run_controller();
	}do_action("_wprdb_deliver_deals");
	
	add_action('admin_menu', 'wprdb_admin_menu');
	
}


/*
Function to define the administration menu.
*/
function wprdb_admin_menu()
{
	add_menu_page('Deal Series','Deal Series','activate_plugins',__FILE__);
	add_submenu_page(__FILE__,'Deal Series','Deal Series','activate_plugins',__FILE__,"_wprdb_deal_series");
}



function wprdb_nodd_found()
{
	if (current_user_can("activate_plugins"))
	{
?>

<div class="error fade">
<p><strong>
<big>WP Autoresponder to Daily Deals Bridge:</big></strong> The currently active theme is not DailyDeal. Please install the theme and set it as the current theme to be able to post daily deals to be delivered by the bridge.
</p>
</div>
<?php
         }
}

function _wprdb_deal_series()
{
        if (preg_match("@\\.@",_wprdb_get("_wprdb_view")))
  	   _wprdb_set("_wprdb_view","deals_home");
	_wprdb_render_view();
}


function wprdb_nowpr_found()
{

	if (current_user_can("activate_plugins"))
	{
?>
<div class="error fade">
<p><strong>
<big>WP Autoresponder to Daily Deals Bridge:</big></strong> The WP Autoresponder plugin is not active. New subscribers will not be added and new deals will not be processed or delivered by e-mail. Please activate WP Autoresponder or deactivate the bridge plugin.
</p>
</div>
<?php
         }

}
register_activation_hook(__FILE__,"wprdb_activation_hook");

function wprdb_activation_hook()
{
     
     global $wpdb;
     //set up the database table. 
     $createDealSubscriptionsTable = sprintf("CREATE TABLE IF NOT EXISTS `%sdeal_email_subscriptions` (
						  `id` int(11) NOT NULL AUTO_INCREMENT,
						  `sid` int(11) NOT NULL,
						  `deal_category` int(11) NOT NULL,
						  `last_published_date` bigint(20) NOT NULL,
						  `last_processed_date` bigint(20) NOT NULL,
						  `date_of_creation` int(11) NOT NULL,
						  PRIMARY KEY (`id`),
						  UNIQUE KEY `unique_subscrtipion_combinations` (`sid`,`deal_category`)
						)",$wpdb->prefix);

	$wpdb->query($createDealSubscriptionsTable);
	
	$createDealDeliveryRecords = sprintf("CREATE TABLE IF NOT EXISTS `%sdeal_email_deliveries` (
					  `sid` int(11) NOT NULL,
					  `deal_id` int(11) NOT NULL,
					  `time_of_dispatch` int(11) NOT NULL,
					  UNIQUE KEY `sid` (`sid`,`deal_id`)
					)",$wpdb->prefix);

	$wpdb->query($createDealDeliveryRecords);
	
	$createDealDealSeries = sprintf("CREATE TABLE IF NOT EXISTS `%sdeal_email_series` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `name` varchar(100) NOT NULL,
					  `deal_category` int(100) NOT NULL,
					  `period` int(11) NOT NULL,
					  PRIMARY KEY (`id`)
					)",$wpdb->prefix);
					
	$wpdb->query($createDealDealSeries);
					
	$createDealFormRelation = sprintf("CREATE TABLE IF NOT EXISTS `%sdeal_email_forms` (
					  `form_id` int(11) NOT NULL,
					  `category_id` int(11) NOT NULL,
					  PRIMARY KEY (`form_id`,`category_id`))",$wpdb->prefix);

	$wpdb->query($createDealFormRelation);
	
	
     
     //if the crons aren't already scheduled, schedule them
     $whetherRelatedCronUnscheduled = (false == wp_get_schedule("_wprdb_deliver_deals")); 
     if (true == $whetherRelatedCronUnscheduled)
     {
     	$anHourFromNow = time()+3600;
     	wp_schedule_event($anHourFromNow,"hourly","_wprdb_deliver_deals");
     }
     
     if (true == $whetherRelatedCronUnscheduled)
     {
     	$anHourFromNow = time()+3600;
     	wp_schedule_event($anHourFromNow,"daily","_wprdb_maintain_stuff");
     }     
}


//creation of subscription form. the following section provides the 
//options that will show in the subscription form creation interface

add_filter("_wpr_subscription_form_followup_options","_wprdb_followup_options",10,2);

function _wprdb_followup_options($types,$form_id=0)
{
    global $wpdb;
    $whetherToSetSelectedItem = false;
    //if the form is a post, then set the selected value to the appropriate deal series listing
    //ensure that this plugin doesn't override any setting by the main WP Autoresponder plugin
    //set while it was deactivated.

    $getIdsOfDealSeriesQuery = sprintf("SELECT id FROM %sdeal_email_series",$wpdb->prefix);
    $listOfDealSeriesIds = $wpdb->get_col($getIdsOfDealSeriesQuery);
    
	$getFormRelationQuery = sprintf("SELECT * FROM %sdeal_email_forms WHERE form_id=%d",$wpdb->prefix,$form_id);

	
	$formResults = $wpdb->get_results($getFormRelationQuery);
	if (0 != count($formResults))
	{
	  $deal_series = $formResults[0]->category_id;
	  
	  if (in_array($deal_series, $listOfDealSeriesIds))
	  {  
                $whetherToSetSelectedItem=true;
		$assignedDealSeriesId = $deal_series;
	  }	    
	}

    
    $getDealSeriesQuery = sprintf("SELECT * FROM %sdeal_email_series",$wpdb->prefix);
    $dealsResult = $wpdb->get_results($getDealSeriesQuery);
    $types['Deal Series'] = array();
    $listOfDealSeriesIds = array();
    foreach ($dealsResult as $dealcat)
    {
	$item = array("id"=>"dealseries_".$dealcat->id,
		      "name"=> $dealcat->name,
		      "selected" => false);
	
        $types['Deal Series'][]=$item;


	if ($whetherToSetSelectedItem == true && $assignedDealSeriesId == $dealcat->id)
	{
	     $indexOfCurrentItem = count($types['Deal Series'])-1;
	     $types['Selected'] = &$types['Deal Series'][$indexOfCurrentItem];
	}
    }
    return $types;
}

//handle the post event when the user creates the subscription form
add_action("_wpr_subscriptionform_created_handler_save","_wprdb_subscriptionform_change_handler",10,1);
add_filter("_wpr_subscriptionform_edit_handler_save","_wprdb_subscriptionform_change_handler",10,2);

function _wprdb_subscriptionform_change_handler($form_id)
{
    global $wpdb;
    //check if the subscription type is a dealseries
    
    if (preg_match("@dealseries_[0-9]+@",$_POST['followup']))
    {
        $dealSeriesId = intval(str_replace("dealseries_","",$_POST['followup']));
	//check if this deal series exists.
	$checkWhetherDealSeriesExistsQuery=sprintf("SELECT * FROM %sdeal_email_series WHERE id=%d",$wpdb->prefix,$dealSeriesId);
	$dealSeriesResults = $wpdb->get_results($checkWhetherDealSeriesExistsQuery);
	if (0 != count($dealSeriesResults))
	{
		//delete existing deal email series
		$deleteRelationQuery = sprintf("DELETE FROM %sdeal_email_forms WHERE form_id=%d",$wpdb->prefix,$form_id);
		$wpdb->query($deleteRelationQuery);
		
		$addRelationQuery = sprintf("INSERT INTO %sdeal_email_forms (form_id, category_id) VALUES  (%d,%d);",$wpdb->prefix,$form_id, $dealSeriesId);
		$wpdb->query($addRelationQuery);
	}
    }
    else
    {
	$deleteExistingSubscriptionIfAnyQuery = sprintf("DELETE FROM %sdeal_email_forms WHERE form_id=%d",$wpdb->prefix,$form_id);
	$wpdb->query($deleteExistingSubscriptionIfAnyQuery);
    }
}


add_action("_wpr_subscription_form_code_generate_hidden_fields","_wprdb_subscriptionform_fields",10,1);


//if the form is one of the forms that is supposed to get a follow-up deal series, then add the hidden fields

function _wprdb_subscriptionform_fields($form)
{
	global $wpdb;
     if ($form->followup_type != "none")
         return;
     //check whether the subscription form has any deal series against it (that exists). A join to ensure that the deal series exists.
     $checkWhetherFormDefinedQuery = sprintf("SELECT * FROM `%sdeal_email_forms` f, `%sdeal_email_series` s WHERE f.`form_id`= %d AND f.`category_id`=s.`id`;",$wpdb->prefix,$wpdb->prefix, $form->id);
     $results = $wpdb->get_results($checkWhetherFormDefinedQuery);
     $row = $results[0];
     if (0 == count($results))
     	return;
     ?>
<!--subscription to deal series-->
<input type="hidden" name="deal_series" value="<?php echo $row->category_id; ?>"/>
     <?php
}

add_action("_wpr_subscriber_added","_wprdb_subscriber_added", $id);

function _wprdb_subscriber_added($id)
{
	global $wpdb;
     //check if the subscription form has a deal_series option
     $dealSeriesField = intval($_POST['deal_series']);
     if ($dealSeriesField == 0)
         return;
     //ensure that the deal series exists.
     $deleteAnyExistingQuery = sprintf("DELETE FROM %sdeal_email_subscriptions WHERE sid=%d",$wpdb->prefix,$id);
     $wpdb->query($deleteAnyExistingQuery);
     $subscriberToDealSeriesQuery = sprintf("INSERT INTO %sdeal_email_subscriptions (sid, deal_category, date_of_creation) VALUES (%d, %d, UNIX_TIMESTAMP());",$wpdb->prefix, $id, $dealSeriesField);
     $wpdb->query($subscriberToDealSeriesQuery);
}
