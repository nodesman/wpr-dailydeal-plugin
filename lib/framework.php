<?php
function _wprdb_get($name)
{
        global $wprdb_globals;
        if (isset($wprdb_globals[$name]))
                  return $wprdb_globals[$name];
        else
                return null;
}

function _wprdb_set($name,$value)
{
    global $wprdb_globals;
    $wprdb_globals[$name] = $value;
}

function _wprdb_handle_post()
{
        if (count($_POST)>0 && isset($_POST['wprdb_form']))
        {
            $formName = $_POST['wprdb_form'];
            $actionName = "_wprdb_".$formName."_post";
            $default_handler_name = $actionName."_handler";
            if (function_exists($default_handler_name))
              add_action($actionName,$default_handler_name);
            do_action($actionName);
        }
}


function _wprdb_run_controller()
{
    $page = $_GET['page'];
    $parts = explode("/",$page);
    $action = $parts[1];
    if ($action == "dealsbridge.php")
    {
       $action = "deal_series_home";
    }
    $arguments= array_splice($parts,1, count($parts));
    $actionName = "_wprdb_".$action."_handle";
    _wprdb_set("_wprdb_view",$action);
    do_action($actionName,$arguments);
}


function _wprdb_render_view()
{
        global $wprdb_globals;
        $plugindir = WPRDB_PLUGIN_DIR;
        $currentView = _wprdb_get("_wprdb_view");              
        foreach ($wprdb_globals as $name=>$value)
        {
                ${$name} = $value;
        }
        $viewfile ="$plugindir/views/".$currentView.".php";
        if (is_file($viewfile))
        include $viewfile;
        foreach ($wprdb_globals as $name=>$value)
        {
                unset(${$name});
        }
}

